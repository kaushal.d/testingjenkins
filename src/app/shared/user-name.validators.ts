import { AbstractControl, ValidatorFn } from "@angular/forms";

export function forbidenNameValidator(forbidenName: RegExp): ValidatorFn{
    return (control: AbstractControl):{[key: string]:any} | null => {
        const forbiden = forbidenName.test(control.value);
        return forbiden ? {'forbidenName': {value: control.value}}: null;
    };
}

export function numberValidator (c: AbstractControl):{[key: string]:any} | null {
    if (c.value && (isNaN(c.value) || c.value > 9999999999 || c.value < 1000000000)) {
        return { 'range': true };
    }
    return null;
}

export function emailValidator (c: AbstractControl):{[key: string]:any} | null {
    if (c.value && (isNaN(c.value) || c.value > 9999999999 || c.value < 1000000000)) {
        return { 'range': true };
    }
    return null;
}

