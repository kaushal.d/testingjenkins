import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { forbidenNameValidator, numberValidator} from '../shared/user-name.validators';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(private fb: FormBuilder) { }

  registrationForm = this.fb.group({
    firstName: ['', [Validators.required, Validators.minLength(3), forbidenNameValidator(/admin/)]],
    lastName: ['', [Validators.required, Validators.minLength(4)]],        
    phoneNo: ['', [Validators.required, numberValidator]],
    emailId: ['', [Validators.required, Validators.email]]/*,
    addressForm: this.fb.group({
      address: ['', [Validators.required, Validators.maxLength(15)]],
      city: ['', [Validators.required, Validators.maxLength(15)]],
      state: ['', [Validators.required, Validators.maxLength(15)]],
      postalCode: ['', [Validators.required, Validators.maxLength(4)]]
    })*/
  });

  ngOnInit() {
  }

  onSubmit(){
    console.log(this.registrationForm.value);
  }

}
